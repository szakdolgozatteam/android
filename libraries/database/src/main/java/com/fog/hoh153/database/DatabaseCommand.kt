package com.fog.hoh153.database

enum class DatabaseCommand {
    LOGIN,
    PROFILE,
    PUSH_NOTIFICATION,
    NEXT_EVENT
}