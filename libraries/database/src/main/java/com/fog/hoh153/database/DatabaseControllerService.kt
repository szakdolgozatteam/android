package com.fog.hoh153.database

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.HandlerThread
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.widget.Toast
import com.fog.hoh153.database.callback.DatabaseInitCallback
import com.fog.hoh153.database.event.DatabaseCommandEvent
import com.fog.hoh153.database.requestdata.*
import com.fog.hoh153.database.runnable.*
import com.fog.hoh153.logger.Logging
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.sql.Connection

class DatabaseControllerService : Service(), DatabaseInitCallback {
    companion object {
        private val log = Logging.getLogger(DatabaseControllerService::class.java)

        private var databaseConnection: Connection? = null

        private var isRunning = false

        fun isRunning(): Boolean = isRunning

        fun getConnection(): Connection? {
            return databaseConnection
        }

        @JvmStatic
        fun sendRequestToDatabase(command: DatabaseCommand, data: RequestData) {
            EventBus.getDefault().post(DatabaseCommandEvent(command, data))
        }
    }

    private val databaseHandlerThread = HandlerThread("DatabaseHandlerThread")
    private val databaseHandler by lazy {
        databaseHandlerThread.start()
        Handler(databaseHandlerThread.looper)
    }

    override fun onCreate() {
        log.debug("onCreate-Begin")
        super.onCreate()
        isRunning = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(1, createNotificationForService(this, DatabaseControllerService::class.java))
        }
        EventBus.getDefault().register(this)
        databaseHandler.post(InitRunnable(this))
        log.debug("onCreate-End")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onDestroy() {
        log.debug("onDestroy-Begin")
        isRunning = false
        databaseHandler.removeCallbacksAndMessages(null)
        databaseHandlerThread.quitSafely()
        EventBus.getDefault().unregister(this)
        super.onDestroy()
        log.debug("onDestroy-End")
    }

    private fun createNotificationForService(context: Context, clazz: Class<*>): Notification? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val intent = Intent(context, clazz)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            return NotificationCompat.Builder(context, createNotificationChannelForService(notificationManager, clazz.name))
                    .setOngoing(true)
                    .setPriority(NotificationCompat.PRIORITY_MIN)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build()
        }
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannelForService(notificationManager: NotificationManager, className: String): String {
        val channelId = className + "Notification"
        val channel = NotificationChannel(className + "Notification", className, NotificationManager.IMPORTANCE_NONE)
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        notificationManager.createNotificationChannel(channel)
        return channelId
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onInitSuccess(connection: Connection) {
        databaseConnection = connection
        log.debug("Database initialized!")
    }

    override fun onInitFailure() {
        Toast.makeText(this, "Adatbázis betöltési hiba…", Toast.LENGTH_SHORT).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDatabaseCommandEvent(event: DatabaseCommandEvent) {
        when (event.command) {
            DatabaseCommand.LOGIN -> databaseHandler.post(LoginRunnable(event.data as LoginRequestData))
            DatabaseCommand.PROFILE -> databaseHandler.post(ProfileRunnable(event.data as ProfileRequestData))
            DatabaseCommand.PUSH_NOTIFICATION -> databaseHandler.post(PushNotificationRunnable(event.data as PushNotificationRequestData))
            DatabaseCommand.NEXT_EVENT -> databaseHandler.post(NextEventRunnable(event.data as NextEventRequestData))
        }
    }

}