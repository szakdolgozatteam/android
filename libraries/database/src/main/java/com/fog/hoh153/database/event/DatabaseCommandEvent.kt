package com.fog.hoh153.database.event

import com.fog.hoh153.database.DatabaseCommand
import com.fog.hoh153.database.requestdata.RequestData

data class DatabaseCommandEvent(val command: DatabaseCommand, val data: RequestData)