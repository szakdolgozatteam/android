package com.fog.hoh153.database.resultdatacontainer

data class LoginResult(val result: Result, val userId: Int = -1, val courses: String = "") : ResultDataContainer() {

    enum class Result {
        SUCCESS,
        FAILURE
    }
}