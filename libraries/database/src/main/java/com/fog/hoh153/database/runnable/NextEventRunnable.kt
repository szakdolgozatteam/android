package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.SqlQuery
import com.fog.hoh153.database.requestdata.NextEventRequestData
import com.fog.hoh153.database.resultdatacontainer.NextEventResult
import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer
import com.fog.hoh153.utils.NextEvent

class NextEventRunnable(private val requestData: NextEventRequestData) : DatabaseRunnable() {
    override fun runnable(): ResultDataContainer {
        val nextEventList: ArrayList<NextEvent> = ArrayList()
        try {
            log.debug("Next Events request sending…")
            get(SqlQuery.nextEventQuery(requestData))?.run {
                while (next()) {
                    log.debug("${getString(1)} : ${getString(2)}")
                    nextEventList.add(NextEvent(getString(1), getString(2)))
                }
            }
        } catch (ignored: Exception) {
            //ignored
        }
        return NextEventResult(nextEventList)
    }

}