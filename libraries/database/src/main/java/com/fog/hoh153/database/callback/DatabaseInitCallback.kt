package com.fog.hoh153.database.callback

import java.sql.Connection

interface DatabaseInitCallback {
    fun onInitSuccess(connection: Connection)
    fun onInitFailure()
}