package com.fog.hoh153.database.resultdatacontainer

data class ErrorResult(val type: Type) : ResultDataContainer() {
    enum class Type {
        NETWORK,
        OTHER
    }
}