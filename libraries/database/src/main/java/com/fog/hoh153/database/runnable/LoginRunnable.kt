package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.SqlQuery
import com.fog.hoh153.database.requestdata.LoginRequestData
import com.fog.hoh153.database.resultdatacontainer.LoginResult
import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer

class LoginRunnable(private val requestData: LoginRequestData) : DatabaseRunnable() {

    override fun runnable(): ResultDataContainer {
        log.debug("Login request sending…")
        try {
            get(SqlQuery.loginQuery(requestData))?.run {
                while (next()) {
                    if (getString(3).toString() == requestData.pass
                            && getString(2).toString() == requestData.name) {
                        return LoginResult(LoginResult.Result.SUCCESS, getInt(1), getString(4))
                    }
                }
            }
        } catch (ignored: Exception) {
            //unused
        }
        return LoginResult(LoginResult.Result.FAILURE)
    }
}