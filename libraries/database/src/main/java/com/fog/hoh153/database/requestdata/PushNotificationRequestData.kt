package com.fog.hoh153.database.requestdata

data class PushNotificationRequestData(val userCourses: MutableList<String>) : RequestData()