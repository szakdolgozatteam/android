package com.fog.hoh153.database.resultdatacontainer

import com.fog.hoh153.utils.NextEvent

data class NextEventResult(val nextEventList: ArrayList<NextEvent>) : ResultDataContainer()