package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.DatabaseControllerService
import com.fog.hoh153.database.event.DatabaseResultEvent
import com.fog.hoh153.database.resultdatacontainer.ErrorResult
import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer
import com.fog.hoh153.logger.CustomLogger
import com.fog.hoh153.logger.Logging
import org.greenrobot.eventbus.EventBus
import java.sql.Connection
import java.sql.ResultSet

abstract class DatabaseRunnable : Runnable {
    companion object {
        val log: CustomLogger = Logging.getLogger(LoginRunnable::class.java)
    }

    private val bus = EventBus.getDefault()
    private var connection: Connection? = null

    override fun run() {
        try {
            connection = DatabaseControllerService.getConnection()
            connection?.run {
                if (!isClosed) {
                    bus.post(DatabaseResultEvent(runnable()))
                }
            }
        } catch (e: Exception) {
            bus.post(createError())
        }
    }

    internal fun get(query: String): ResultSet? {
        return connection?.run {
            return@run createStatement().executeQuery(query)
        }
    }

    private fun createError(): DatabaseResultEvent {
        return DatabaseResultEvent(ErrorResult(ErrorResult.Type.NETWORK))
    }

    abstract fun runnable(): ResultDataContainer
}