package com.fog.hoh153.database.resultdatacontainer

import com.fog.hoh153.utils.Notification

data class PushNotificationResult(val notifications: ArrayList<Notification>) : ResultDataContainer()