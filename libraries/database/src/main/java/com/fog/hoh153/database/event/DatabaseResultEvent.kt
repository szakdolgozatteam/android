package com.fog.hoh153.database.event

import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer

data class DatabaseResultEvent(val result: ResultDataContainer)