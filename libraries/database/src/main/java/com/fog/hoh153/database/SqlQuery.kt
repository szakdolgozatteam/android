package com.fog.hoh153.database

import com.fog.hoh153.database.requestdata.LoginRequestData
import com.fog.hoh153.database.requestdata.NextEventRequestData
import com.fog.hoh153.database.requestdata.ProfileRequestData
import com.fog.hoh153.database.requestdata.PushNotificationRequestData

object SqlQuery {

    fun loginQuery(data: LoginRequestData) = "SELECT user_id, user_name, password, GROUP_CONCAT(active_education)" +
            " FROM user JOIN students ON students.userid = user.user_id " +
            "JOIN education_students ON students.student_id = education_students.student_id" +
            "WHERE user_name = \"${data.name}\" AND password = \"${data.pass}\""

    fun profileQuery(data: ProfileRequestData) = "SELECT " +
            "student_full_name, birth_place, birth_date, mothers_name, gender, nationality, phone_number, home_address" +
            " FROM students WHERE userid = \"${data.userId}\""

    fun pushNotificationQuery(data: PushNotificationRequestData): String {
        var resString = "SELECT message,date FROM push_notice WHERE "
        data.userCourses.forEachIndexed { index, s ->
            resString += "schedule_plan_data_id = \"$s\""
            if (index < data.userCourses.size - 1) {
                resString += " OR "
            }
        }
        return resString
    }

    fun nextEventQuery(data: NextEventRequestData): String {
        var resString = "SELECT modul_name,date FROM schedule_plan JOIN modul ON modul.modul_id = schedule_plan.used_modul_id WHERE "
        data.userCourses.forEachIndexed { index, s ->
            resString += "schedule_plan_data_id = \"$s\""
            if (index < data.userCourses.size - 1) {
                resString += " OR "
            }
        }
        resString += " AND date >= \"${data.date}\" ORDER BY date ASC LIMIT 10"
        return resString
    }
}