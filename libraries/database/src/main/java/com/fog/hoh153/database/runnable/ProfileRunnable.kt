package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.SqlQuery
import com.fog.hoh153.database.requestdata.ProfileRequestData
import com.fog.hoh153.database.resultdatacontainer.ErrorResult
import com.fog.hoh153.database.resultdatacontainer.ProfileResult
import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer

class ProfileRunnable(private val requestData: ProfileRequestData) : DatabaseRunnable() {
    override fun runnable(): ResultDataContainer {
        log.debug("Profile request sending…")
        try {
            get(SqlQuery.profileQuery(requestData))?.run {
                while (next()) {
                    return ProfileResult(
                            getString(1),
                            getString(2),
                            getString(3),
                            getString(4),
                            getString(5),
                            getString(6),
                            getString(7),
                            getString(8))
                }
            }
        } catch (e: Exception) {
        }
        return ErrorResult(ErrorResult.Type.OTHER)
    }
}