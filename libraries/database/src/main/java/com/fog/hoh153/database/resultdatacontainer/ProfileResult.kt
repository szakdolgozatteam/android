package com.fog.hoh153.database.resultdatacontainer

data class ProfileResult(val fullName: String,
                         val birthPlace: String,
                         val birthTime: String,
                         val mothersName: String,
                         val gender: String,
                         val nationality: String,
                         val phoneNumber: String,
                         val address: String)
    : ResultDataContainer()