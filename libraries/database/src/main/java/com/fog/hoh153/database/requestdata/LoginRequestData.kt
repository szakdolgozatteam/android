package com.fog.hoh153.database.requestdata

data class LoginRequestData(val name: String, val pass: String) : RequestData()