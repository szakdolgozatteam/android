package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.SqlQuery
import com.fog.hoh153.database.requestdata.PushNotificationRequestData
import com.fog.hoh153.database.resultdatacontainer.PushNotificationResult
import com.fog.hoh153.database.resultdatacontainer.ResultDataContainer
import com.fog.hoh153.utils.Notification

class PushNotificationRunnable(private val requestData: PushNotificationRequestData) : DatabaseRunnable() {
    override fun runnable(): ResultDataContainer {
        val notificationList: ArrayList<Notification> = ArrayList()
        try {
            get(SqlQuery.pushNotificationQuery(requestData))?.run {
                while (next()) {
                    notificationList.add(Notification(getString(1), getString(2)))
                }
            }
        } catch (ignored: Exception) {
            //ignored
        }
        return PushNotificationResult(notificationList)
    }
}