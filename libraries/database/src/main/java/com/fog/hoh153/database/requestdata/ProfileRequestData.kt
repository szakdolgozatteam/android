package com.fog.hoh153.database.requestdata

data class ProfileRequestData(val userId: Int) : RequestData()