package com.fog.hoh153.database.requestdata

data class NextEventRequestData(val userCourses: MutableList<String>, val date: String) : RequestData()