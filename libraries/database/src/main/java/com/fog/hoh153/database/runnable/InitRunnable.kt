package com.fog.hoh153.database.runnable

import com.fog.hoh153.database.callback.DatabaseInitCallback
import com.fog.hoh153.logger.Logging
import java.sql.DriverManager

class InitRunnable(private val callback: DatabaseInitCallback) : Runnable {
    companion object {
        private val log = Logging.getLogger(InitRunnable::class.java)

        private const val DATABASE_CLASS = "com.mysql.jdbc.Driver"
        private const val DATABASE_URL = "jdbc:mysql://mysql.nethely.hu:3306/oktat"
        private const val DATABASE_USERNAME = "oktat"
        private const val DATABASE_PASSWORD = "corvin2019"
    }

    override fun run() {
        log.debug("Initializing database connection…")
        try {
            Class.forName(DATABASE_CLASS)
            callback.onInitSuccess(DriverManager.getConnection(DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD))
        } catch (e: Exception) {
            log.error(e)
            callback.onInitFailure()
        }
    }
}