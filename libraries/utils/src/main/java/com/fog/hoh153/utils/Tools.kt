package com.fog.hoh153.utils

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Looper
import android.widget.Toast
import com.fog.hoh153.logger.Logging
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.util.*

object Tools {

    private val log = Logging.getLogger(Tools::class.java)

    private const val FACEBOOK_PACKAGE_NAME = "com.facebook.katana"
    private const val FACEBOOK_URL = "fb://facewebmodal/f?href="

    fun checkPermissions(activity: Activity) {
        try {
            val permissions: Array<String>
            val packageInfo = activity.packageManager.getPackageInfo(activity.packageName, PackageManager.GET_PERMISSIONS)
            permissions = packageInfo.requestedPermissions

            getDangerousPermissions(activity, object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (!report.areAllPermissionsGranted()) {
                        val leftoverPermissions = ArrayList<String>()
                        for (r in report.grantedPermissionResponses) {
                            leftoverPermissions.add(r.permissionName)
                        }
                        recheckPermissions(leftoverPermissions, activity)
                    }
                }
                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }, listOf(*permissions))
        } catch (e: PackageManager.NameNotFoundException) {
            log.error(e)
        }
    }

    private fun getDangerousPermissions(activity: Activity, permissionListener: MultiplePermissionsListener, permissions: List<String>) {
        Dexter.withActivity(activity)
                .withPermissions(permissions)
                .withListener(permissionListener)
                .check()
    }

    private fun recheckPermissions(permissions: List<String>, activity: Activity) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Toast.makeText(activity, "Not all needed permissions were granted! Retrying after 3 seconds...", Toast.LENGTH_SHORT).show()
        } else {
            log.info("Not all needed permissions were granted! Retrying after 3 seconds...")
        }
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                getDangerousPermissions(activity, object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (!report.areAllPermissionsGranted()) {
                            val leftoverPermissions = ArrayList<String>()
                            for (r in report.grantedPermissionResponses) {
                                leftoverPermissions.add(r.permissionName)
                            }
                            recheckPermissions(leftoverPermissions, activity)
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }, permissions)
            }
        }, 3000)
    }

    fun createFacebookUrl(pm: PackageManager, url: String): Intent {
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo(FACEBOOK_PACKAGE_NAME, 0)
            if (applicationInfo.enabled) {
                uri = Uri.parse(FACEBOOK_URL + url)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        return Intent(Intent.ACTION_VIEW, uri)
    }
}
