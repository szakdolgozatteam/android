package com.fog.hoh153.utils

object PreferenceKeys {
    const val USER_ID = "userId"
    const val USER_COURSES = "userCourses"
    const val LAST_LOGIN_TIME = "lastLoginTime"
    const val USER_PASSWORD = "password"
}
