package com.fog.hoh153.utils

data class NextEvent(val event: String, val date: String)