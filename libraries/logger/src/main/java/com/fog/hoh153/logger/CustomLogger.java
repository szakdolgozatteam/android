package com.fog.hoh153.logger;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;

import org.apache.log4j.Logger;

@SuppressWarnings("WeakerAccess,unused")
public class CustomLogger {

    private static final String DEFAULT_TAG = "uninitialized";
    private static final String DEFAULT_NULL_MESSAGE = "null";

    private static final HandlerThread logHandlerThread;
    private static final Handler logHandler;

    static {
        logHandlerThread = new HandlerThread(".log.CustomLogger$LoggerHandlerThread");
        logHandlerThread.start();
        logHandler = new Handler(logHandlerThread.getLooper());
    }

    private final Logger log;

    CustomLogger(Logger log) {
        this.log = log;
    }

    public void trace(Object message) {
        if (message instanceof Throwable) {
            trace("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.trace(message));
            } else {
                Log.v(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }
        }
    }

    public void trace(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.trace(message, t));
        } else {
            Log.v(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }
    }

    public void debug(Object message) {
        if (message instanceof Throwable) {
            debug("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.debug(message));
            } else {
                Log.d(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }
        }
    }

    public void debug(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.debug(message, t));
        } else {
            Log.d(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }
    }

    public void info(Object message) {
        if (message instanceof Throwable) {
            info("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.info(message));
            } else {
                Log.i(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }
        }
    }

    public void info(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.info(message, t));
        } else {
            Log.i(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }
    }

    public void warn(Object message) {
        if (message instanceof Throwable) {
            warn("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.warn(message));
            } else {
                Log.w(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }
        }
    }

    public void warn(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.warn(message, t));
        } else {
            Log.w(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }
    }

    public void error(Object message) {
        if (message instanceof Throwable) {
            error("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.error(message));
            } else {
                Log.e(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }

        }
    }

    public void error(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.error(message, t));
        } else {
            Log.e(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }

    }

    public void fatal(Object message) {
        if (message instanceof Throwable) {
            fatal("", (Throwable) message);
        } else {
            if (Logging.initialized) {
                logHandler.post(() -> log.fatal(message));
            } else {
                Log.wtf(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE);
            }

        }
    }

    public void fatal(Object message, Throwable t) {
        if (Logging.initialized) {
            logHandler.post(() -> log.fatal(message, t));
        } else {
            Log.wtf(DEFAULT_TAG, message != null ? message.toString() : DEFAULT_NULL_MESSAGE, t);
        }
    }

}
