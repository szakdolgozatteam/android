package com.fog.hoh153.logger;

public interface LogListener {
    void onLogMessage(final String logMessage);
}
