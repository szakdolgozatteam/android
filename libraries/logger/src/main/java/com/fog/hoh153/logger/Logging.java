package com.fog.hoh153.logger;

import android.support.annotation.NonNull;
import android.util.Log;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import de.mindpipe.android.logging.log4j.LogConfigurator;

@SuppressWarnings({"UnusedReturnValue", "unused"})
public class Logging {

    private static final Map<String, StatusAppender> appenderList = new HashMap<>();

    public static String logFilePath;

    public static File logFile;

    static boolean initialized = false;

    private static Level logLevel = Level.ALL;

    public static CustomLogger getLogger(Class<?> cls) {
        init();
        return new CustomLogger(Logger.getLogger(cls));
    }

    public static void init() {
        if (!initialized) {
            try {
                if (logFile == null) {
                    if (logFilePath != null) {
                        logFile = new File(logFilePath);
                    } else {
                        throw new IOException("uninitialized log file path");
                    }
                }
                if (!logFile.exists()) {
                    File parentFile = logFile.getParentFile();
                    if (parentFile != null && !parentFile.exists()) {
                        if (!parentFile.mkdirs()) {
                            throw new IOException("creating parent folder for log file is unsuccessful");
                        }
                    }
                    if (!logFile.createNewFile()) {
                        throw new IOException("creating new log file is unsuccessful");
                    }
                }
                final LogConfigurator config = new LogConfigurator();
                config.setRootLevel(logLevel);
                config.setFileName(logFile.getAbsolutePath());
                config.setFilePattern("%d %p %c{1} - %m%n");
                config.setMaxFileSize(15728640);
                config.setMaxBackupSize(0);
                config.configure();
                final Logger root = Logger.getRootLogger();
                for (final Map.Entry<String, StatusAppender> appender : appenderList.entrySet()) {
                    root.addAppender(appender.getValue());
                }
                initialized = true;
                Log.d(Logging.class.getName(), "Logger configured successfully.");
            } catch (Exception e) {
                String message = e.getMessage();
                if (message != null) {
                    Log.e(Logging.class.getName(), "Failed to create log file: " + message);
                } else {
                    Log.e(Logging.class.getName(), "Failed to create log file.", e);
                }
            }
        }
    }

    public static synchronized boolean deleteLog() {
        if (logFile == null) {
            return false;
        }
        boolean success = false;
        if (logFile.delete()) {
            success = true;
            initialized = false;
        } else {
            Log.w(Logging.class.getName(), "Couldn't delete log file.");
        }
        return success;
    }

    public static synchronized String addLogListener(final LogListener listener) {
        init();
        StatusAppender appender = new StatusAppender(listener);
        String id = UUID.randomUUID().toString();
        appenderList.put(id, appender);
        Logger.getRootLogger().addAppender(appender);
        return id;
    }

    public static synchronized void removeLogListener(final String listenerId) {
        StatusAppender appender = appenderList.remove(listenerId);
        appender.close();
        Logger.getRootLogger().removeAppender(appender);
    }

    public static synchronized void destroy() {
        LogManager.shutdown();
        Enumeration e = Logger.getRootLogger().getAllAppenders();
        while (e.hasMoreElements()) {
            ((StatusAppender) e.nextElement()).close();
        }
        appenderList.clear();
        Logger.getRootLogger().removeAllAppenders();
        initialized = false;
    }

    public static void setLogLevel(@NonNull final Level logLevel) {
        Logging.logLevel = logLevel;
        Logger.getRootLogger().setLevel(logLevel);
    }

    public static String getLogFilePath() {
        if (logFile == null) {
            return null;
        }
        return logFile.getAbsolutePath();
    }

}
