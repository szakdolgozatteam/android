package com.corvin.client

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.arch.lifecycle.ProcessLifecycleOwner
import android.content.Intent
import android.os.Build
import android.os.Environment
import android.support.multidex.MultiDexApplication
import com.fog.hoh153.database.DatabaseControllerService
import com.fog.hoh153.logger.Logging
import java.io.File

class Application : MultiDexApplication(), LifecycleObserver {

    init {
        Logging.logFilePath = Environment.getExternalStorageDirectory().toString() + File.separator + "log.log"
        Logging.init()
    }

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onAppBackgrounded() {
        if (DatabaseControllerService.isRunning()) {
            stopService(Intent(this, DatabaseControllerService::class.java))
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onAppForegrounded() {
        if (!DatabaseControllerService.isRunning()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(Intent(this, DatabaseControllerService::class.java))
            } else {
                startService(Intent(this, DatabaseControllerService::class.java))
            }
        }
    }
}
