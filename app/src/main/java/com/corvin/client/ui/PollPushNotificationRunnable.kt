package com.corvin.client.ui

import android.content.Context
import android.os.Handler
import android.preference.PreferenceManager
import com.fog.hoh153.database.DatabaseCommand
import com.fog.hoh153.database.DatabaseControllerService
import com.fog.hoh153.database.requestdata.PushNotificationRequestData
import com.fog.hoh153.utils.PreferenceKeys

class PollPushNotificationRunnable(private val context: Context, private val handler: Handler) : Runnable {
    companion object {
        private const val DELAY_BETWEEN_REQUESTS = 30000L

        private var isRunning = true

        fun setIsRunning(isRunning: Boolean) {
            Companion.isRunning = isRunning
        }
    }

    override fun run() {
        if (isRunning) {
            PreferenceManager.getDefaultSharedPreferences(context).getString(PreferenceKeys.USER_COURSES, null)?.run {
                DatabaseControllerService.sendRequestToDatabase(DatabaseCommand.PUSH_NOTIFICATION,
                        PushNotificationRequestData(split(",").toMutableList()))
            }
            handler.postDelayed(this, DELAY_BETWEEN_REQUESTS)
        }
    }

}