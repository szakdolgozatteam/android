package com.corvin.client.ui.activity

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import com.corvin.client.R
import com.corvin.client.ui.activity.tab.TabPagerAdapter
import com.corvin.client.ui.fragment.LearningFragment
import com.corvin.client.ui.fragment.MainFragment
import com.corvin.client.ui.fragment.ProfileFragment
import com.fog.hoh153.utils.PreferenceKeys
import com.fog.hoh153.utils.Tools
import com.fog.hoh153.logger.Logging

class MainPageActivity : AppCompatActivity() {
    companion object {
        private val log = Logging.getLogger(MainPageActivity::class.java)

        private const val FACEBOOK_URL = "https://www.facebook.com/corvinkozoktatasikozpont/"
    }

    private var backPressCounter: Int = 0
    private val preferences by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    override fun onResume() {
        log.debug("onCreate-Begin")
        super.onResume()
        backPressCounter = 0
        log.debug("onCreate-End")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        log.debug("onCreate-Begin")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tab_layout)

        val moreOptionsButton = findViewById<Button>(R.id.tab_more_options_button)
        val popupMenu = PopupMenu(this@MainPageActivity, moreOptionsButton)
        popupMenu.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.navigation_facebook -> startActivity(Tools.createFacebookUrl(packageManager, FACEBOOK_URL))
                R.id.navigation_sign_out -> {
                    preferences.edit().putLong(PreferenceKeys.LAST_LOGIN_TIME, 0).apply()
                    startActivity(Intent(this@MainPageActivity, AdActivity::class.java))
                }
            }
            return@setOnMenuItemClickListener true
        }
        popupMenu.menuInflater.inflate(R.menu.navigation, popupMenu.menu)
        moreOptionsButton.setOnClickListener {
            popupMenu.show()
        }

        val pager = findViewById<ViewPager>(R.id.view_pager)
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        tabLayout.run {
            val adapter = TabPagerAdapter(supportFragmentManager, 3)
            adapter.add(MainFragment(), getString(R.string.main_tab_main_page))
            adapter.add(ProfileFragment(), getString(R.string.main_tab_profile))
            adapter.add(LearningFragment(), getString(R.string.main_tab_learning))
            pager.adapter = adapter
            setupWithViewPager(pager)
        }
        log.debug("onCreate-End")
    }

    override fun onBackPressed() {
        backPressCounter++
        if (backPressCounter == 1) {
            Toast.makeText(this, getString(R.string.main_activity_press_back_label), Toast.LENGTH_SHORT).show()
        }
        if (backPressCounter >= 2) {
            startActivity(Intent(this, AdActivity::class.java))
            preferences.edit().putLong(PreferenceKeys.LAST_LOGIN_TIME, 0).apply()
        }
    }
}