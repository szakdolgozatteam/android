package com.corvin.client.ui.fragment.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.corvin.client.ui.view.NotificationView
import com.fog.hoh153.utils.Notification

class NotificationAdapter(private val notificationList: ArrayList<Notification>) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val view = NotificationView(viewGroup.context)
        view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = notificationList.size

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.view.setText(notificationList[i].message)
        viewHolder.view.setDate(notificationList[i].date)
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val view: NotificationView = v as NotificationView
    }

}