package com.corvin.client.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import com.corvin.client.R
import com.corvin.client.ui.dialog.LoginDialog
import com.fog.hoh153.database.event.DatabaseResultEvent
import com.fog.hoh153.database.resultdatacontainer.ErrorResult
import com.fog.hoh153.database.resultdatacontainer.LoginResult
import com.fog.hoh153.logger.Logging
import com.fog.hoh153.utils.PreferenceKeys
import com.fog.hoh153.utils.Tools
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class AdActivity : AppCompatActivity() {
    companion object {
        private val log = Logging.getLogger(AdActivity::class.java)

        private const val LAST_LOGIN_THRESHOLD = 1800000L
    }

    private var loginDialog: LoginDialog? = null

    //private val topAdWebView: WebView by lazy { findViewById<WebView>(R.id.Ad_1) }
    //private val bottomAdWebView: WebView by lazy { findViewById<WebView>(R.id.Ad_2) }

    private val preferences by lazy { PreferenceManager.getDefaultSharedPreferences(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        log.debug("onCreate-Begin")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ad)

        supportActionBar?.hide()

        findViewById<Button>(R.id.to_login_button).setOnClickListener {
            loginDialog?.dismissAllowingStateLoss() ?: run { loginDialog = LoginDialog() }
            loginDialog?.run {
                supportFragmentManager.beginTransaction().add(this, null).commitAllowingStateLoss()
            }
        }

        findViewById<Button>(R.id.fodr_butt).setOnClickListener {
            val uri = Uri.parse("https://fodraszkepzes.hu/") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }

        findViewById<Button>(R.id.kozm_butt).setOnClickListener {
            val uri = Uri.parse("https://kozmetikuskepzes.hu/") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
/*
        topAdWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        bottomAdWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }
        }*/
        log.debug("onCreate-End")
    }

    override fun onResume() {
        log.debug("onResume-Begin")
        super.onResume()
        Tools.checkPermissions(this)
        loadWebViews()
        EventBus.getDefault().register(this)
        autoLogin()
        log.debug("onResume-End")
    }

    override fun onPause() {
        log.debug("onPause-Begin")
        EventBus.getDefault().unregister(this)
        super.onPause()
        log.debug("onPause-End")
    }

    private fun loadWebViews() {
        // topAdWebView.loadUrl("https://kozmetikuskepzes.hu/")
        // bottomAdWebView.loadUrl("https://fodraszkepzes.hu/")
    }

    private fun autoLogin() {
        val lastLoginTime = preferences.getLong(PreferenceKeys.LAST_LOGIN_TIME, 0)
        if ((System.currentTimeMillis() - lastLoginTime <= LAST_LOGIN_THRESHOLD)
                && (preferences.getInt(PreferenceKeys.USER_ID, -1) != -1)
                && (preferences.getString(PreferenceKeys.USER_COURSES, null) != null)) {
            preferences.edit().putLong(PreferenceKeys.LAST_LOGIN_TIME, System.currentTimeMillis()).apply()
            startActivity(Intent(this@AdActivity, MainPageActivity::class.java))
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataBaseResultEvent(event: DatabaseResultEvent) {
        loginDialog?.dismissAllowingStateLoss()
        loginDialog = null
        if (event.result is LoginResult) {
            val result = event.result as LoginResult
            if (result.result == LoginResult.Result.SUCCESS) {
                val editor = preferences.edit()
                editor.putInt(PreferenceKeys.USER_ID, result.userId)
                editor.putString(PreferenceKeys.USER_COURSES, result.courses)
                editor.putLong(PreferenceKeys.LAST_LOGIN_TIME, System.currentTimeMillis())
                editor.apply()
                startActivity(Intent(this@AdActivity, MainPageActivity::class.java))
            } else {
                Toast.makeText(this, getString(R.string.ad_uanble_to_login), Toast.LENGTH_SHORT).show()
            }
        } else if ((event.result as ErrorResult).type == ErrorResult.Type.NETWORK) {
            Toast.makeText(this, getString(R.string.communication_error), Toast.LENGTH_SHORT).show()
        }
    }
}
