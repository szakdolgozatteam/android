package com.corvin.client.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.corvin.client.R

import com.ms_square.etsyblur.BlurDialogFragment

class ProgressDialog : BlurDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.run {
            requestFeature(Window.FEATURE_NO_TITLE)
            attributes.windowAnimations = R.style.DialogAnimation
        }
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.progress_layout, container, false)
    }
}
