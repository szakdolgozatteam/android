package com.corvin.client.ui

import android.util.Base64
import com.corvin.client.ui.activity.AdActivity
import com.fog.hoh153.database.event.DatabaseResultEvent
import com.fog.hoh153.database.requestdata.LoginRequestData
import com.fog.hoh153.database.resultdatacontainer.LoginResult
import com.fog.hoh153.logger.Logging
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject

class LoginPhpRunnable(private val url: String, private val requestData: LoginRequestData) : Runnable {
    companion object {
        private val log = Logging.getLogger(AdActivity::class.java)
    }

    private val client = OkHttpClient()

    override fun run() {
        val json = JSONObject()
        json.put("userName", requestData.name)
        json.put("password", Base64.encodeToString(requestData.pass.toByteArray(), Base64.DEFAULT))

        log.debug(json.toString())

        val response = client.newCall(Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json"), json.toString()))
                .build())
                .execute()

        if (response.code() == 200) {
            try {
                val respJson = JSONObject(response.body()?.string())
                if (respJson.opt("ERROR") == null) {
                    val userId = respJson.getInt("userId")
                    val courses = respJson.getString("course")
                    EventBus.getDefault().post(DatabaseResultEvent(LoginResult(LoginResult.Result.SUCCESS, userId, courses)))
                    return
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        EventBus.getDefault().post(DatabaseResultEvent(LoginResult(LoginResult.Result.FAILURE)))

    }

}