package com.corvin.client.ui.fragment

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.corvin.client.R
import com.corvin.client.databinding.FragmentProfileBinding
import com.corvin.client.ui.dialog.ProgressDialog
import com.fog.hoh153.database.DatabaseCommand
import com.fog.hoh153.database.DatabaseControllerService
import com.fog.hoh153.database.event.DatabaseResultEvent
import com.fog.hoh153.database.requestdata.ProfileRequestData
import com.fog.hoh153.database.resultdatacontainer.ErrorResult
import com.fog.hoh153.database.resultdatacontainer.ProfileResult
import com.fog.hoh153.logger.Logging
import com.fog.hoh153.utils.PreferenceKeys
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ProfileFragment : Fragment() {
    companion object {
        private val log = Logging.getLogger(ProfileFragment::class.java)
    }

    private lateinit var dataBinding: FragmentProfileBinding

    private var progressDialog: ProgressDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        log.debug("onCreateView-Begin")
        EventBus.getDefault().register(this)
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        log.debug("onCreateView-End")
        return dataBinding.root
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            val userId = PreferenceManager.getDefaultSharedPreferences(context).getInt(PreferenceKeys.USER_ID, -1)
            if (userId != -1) {
                DatabaseControllerService.sendRequestToDatabase(DatabaseCommand.PROFILE, ProfileRequestData(userId))
                progressDialog = ProgressDialog()
                progressDialog?.run {
                    activity?.supportFragmentManager?.beginTransaction()
                            ?.add(this, null)
                            ?.commitAllowingStateLoss()
                }
            }
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataBaseResultEvent(event: DatabaseResultEvent) {
        progressDialog?.run {
            if (isAdded) {
                dismissAllowingStateLoss()
            }
        }
        progressDialog = null
        if (event.result is ProfileResult) {
            dataBinding.model = event.result as ProfileResult
        } else if (event.result is ErrorResult) {
            if ((event.result as ErrorResult).type == ErrorResult.Type.NETWORK) {
                Toast.makeText(context, getString(R.string.communication_error), Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, getString(R.string.profile_load_fail), Toast.LENGTH_SHORT).show()
            }
        }
    }
}