package com.corvin.client.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.corvin.client.R
import com.fog.hoh153.database.event.DatabaseResultEvent
import com.fog.hoh153.database.resultdatacontainer.NextEventResult
import com.fog.hoh153.database.resultdatacontainer.PushNotificationResult
import com.corvin.client.ui.PollPushNotificationRunnable
import com.corvin.client.ui.fragment.adapter.NextEventAdapter
import com.corvin.client.ui.fragment.adapter.NotificationAdapter
import com.fog.hoh153.database.DatabaseCommand
import com.fog.hoh153.database.DatabaseControllerService
import com.fog.hoh153.database.requestdata.NextEventRequestData
import com.fog.hoh153.utils.NextEvent
import com.fog.hoh153.utils.Notification
import com.fog.hoh153.logger.CustomLogger
import com.fog.hoh153.logger.Logging
import com.fog.hoh153.utils.PreferenceKeys
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainFragment : Fragment() {
    companion object {
        private val log: CustomLogger = Logging.getLogger(MainFragment::class.java)
    }

    private lateinit var notificationContainer: RecyclerView
    private lateinit var nextEventsContainer: RecyclerView

    private val notificationList = ArrayList<Notification>()
    private val notificationAdapter = NotificationAdapter(notificationList)

    private val nextEventList = ArrayList<NextEvent>()
    private val nextEventAdapter = NextEventAdapter(nextEventList)

    private val pushNotificationHandlerThread = HandlerThread("PushNotificationPollThread")
    private val pushNotificationHandler by lazy {
        pushNotificationHandlerThread.start()
        Handler(pushNotificationHandlerThread.looper)
    }

    private var pollPushNotificationRunnable: PollPushNotificationRunnable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        log.debug("onCreateView-Begin")
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        nextEventsContainer = view.findViewById(R.id.main_next_events_container)
        nextEventsContainer.layoutManager = LinearLayoutManager(context)
        nextEventsContainer.itemAnimator = DefaultItemAnimator()
        nextEventsContainer.adapter = nextEventAdapter

        notificationContainer = view.findViewById(R.id.main_notifications_container_view)
        notificationContainer.layoutManager = LinearLayoutManager(context)
        notificationContainer.itemAnimator = DefaultItemAnimator()
        notificationContainer.adapter = notificationAdapter

        EventBus.getDefault().register(this)

        context?.run {
            pollPushNotificationRunnable = PollPushNotificationRunnable(this, pushNotificationHandler)
            pushNotificationHandler.post(pollPushNotificationRunnable)
        }

        PreferenceManager.getDefaultSharedPreferences(context).getString(PreferenceKeys.USER_COURSES, null)?.run {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
            log.debug("SEND")
            DatabaseControllerService.sendRequestToDatabase(DatabaseCommand.NEXT_EVENT, NextEventRequestData(split(",").toMutableList(),
                    format.format(Date(System.currentTimeMillis()))))
        }
        log.debug("onCreateView-End")
        return view
    }

    override fun onPause() {
        log.debug("onPause-Start")
        EventBus.getDefault().unregister(this)
        super.onPause()
        log.debug("onPause-End")
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if (menuVisible) {
            PollPushNotificationRunnable.setIsRunning(true)
            pushNotificationHandler.post(pollPushNotificationRunnable)
        } else {
            PollPushNotificationRunnable.setIsRunning(false)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDataBaseResultEvent(event: DatabaseResultEvent) {
        if (event.result is PushNotificationResult) {
            (event.result as PushNotificationResult).notifications.forEach {
                notificationFindAndAdd(it)
            }
            notificationAdapter.notifyDataSetChanged()
        }
        if (event.result is NextEventResult) {
            (event.result as NextEventResult).nextEventList.forEach {
                log.debug(it)
                nextEventFindAndAdd(it)
            }
            nextEventAdapter.notifyDataSetChanged()
        }
    }

    private fun notificationFindAndAdd(notification: Notification) {
        var found = false
        notificationList.forEach {
            if (it.message == notification.message) {
                found = true
            }
        }
        if (!found) {
            notificationList.add(notification)
        }
    }

    private fun nextEventFindAndAdd(nextEvent: NextEvent) {
        var found = false
        nextEventList.forEach {
            if (it.event == nextEvent.event) {
                found = true
            }
        }
        if (!found) {
            nextEventList.add(nextEvent)
        }
    }
}
