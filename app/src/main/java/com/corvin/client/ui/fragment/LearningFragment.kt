package com.corvin.client.ui.fragment

import android.app.DownloadManager
import android.content.Context.DOWNLOAD_SERVICE
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import com.corvin.client.R
import java.util.regex.Pattern


class LearningFragment : Fragment() {
    companion object {
        private const val MOODLE_URL = "http://elearning.narasoft.hu/my/"

        private val fileNamePattern = Pattern.compile("filename=\"(.*\\.pdf)\"")
    }

    private lateinit var webView: WebView
    private lateinit var progressBarLayout: ConstraintLayout
    private lateinit var backButton: Button
    private lateinit var refreshButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_learning, container, false)
        initWebView(view)
        initButtons(view)
        progressBarLayout = view.findViewById(R.id.learning_progress_layout)
        progressBarLayout.visibility = View.VISIBLE

        webView.loadUrl(MOODLE_URL)
        return view
    }

    private fun initWebView(view: View) {
        webView = view.findViewById(R.id.learning_webview)
        val webSettings = webView.settings
        webSettings.allowFileAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.allowContentAccess = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptEnabled = true
        webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        webSettings.javaScriptCanOpenWindowsAutomatically = false
        webSettings.mediaPlaybackRequiresUserGesture = false

        webSettings.cacheMode = WebSettings.LOAD_NO_CACHE
        webSettings.setAppCacheEnabled(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        webSettings.loadsImagesAutomatically = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBarLayout.visibility = View.GONE
                backButton.visibility = View.VISIBLE
                refreshButton.visibility = View.VISIBLE
                super.onPageFinished(view, url)
            }
        }

        webView.setDownloadListener { url, _, contentDisposition, _, _ ->
            val matcher = fileNamePattern.matcher(contentDisposition)
            if (matcher.find()) {
                val fileName = matcher.group(1)
                val request = DownloadManager.Request(Uri.parse(url)).apply {
                    allowScanningByMediaScanner()
                    setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                }
                (webView.context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager).enqueue(request)
                Toast.makeText(webView.context, "$fileName letöltése…", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun initButtons(view: View) {
        backButton = view.findViewById(R.id.learning_back_button)
        backButton.setOnClickListener {
            webView.goBack()
        }
        refreshButton = view.findViewById(R.id.learning_refresh_button)
        refreshButton.setOnClickListener {
            webView.loadUrl(MOODLE_URL)
        }
    }
}