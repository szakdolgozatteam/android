package com.corvin.client.ui.fragment.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.corvin.client.ui.view.NextEventView
import com.fog.hoh153.utils.NextEvent

class NextEventAdapter(private val nextEventList: ArrayList<NextEvent>) : RecyclerView.Adapter<NextEventAdapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val view = NextEventView(viewGroup.context)
        view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = nextEventList.size

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.view.setEvent(nextEventList[i].event)
        viewHolder.view.setDate(nextEventList[i].date)
    }

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val view: NextEventView = v as NextEventView
    }

}