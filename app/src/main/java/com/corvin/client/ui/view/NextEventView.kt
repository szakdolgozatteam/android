package com.corvin.client.ui.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.widget.TextView
import com.corvin.client.R

class NextEventView(context: Context) : ConstraintLayout(context) {

    private val nextEventText: TextView
    private val nextEventDate: TextView

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.next_event_layout, this)
        nextEventText = findViewById(R.id.next_event_text)
        nextEventDate = findViewById(R.id.next_event_date)
    }

    fun setEvent(text: String) {
        nextEventText.text = text
    }

    fun setDate(date: String) {
        nextEventDate.text = date
    }


}