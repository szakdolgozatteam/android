package com.corvin.client.ui.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.widget.TextView
import com.corvin.client.R

class NotificationView(context: Context) : ConstraintLayout(context) {

    private val notificationText: TextView
    private val notificationDate: TextView

    init {
        val inflater = LayoutInflater.from(context)
        inflater.inflate(R.layout.notification_layout, this)
        notificationText = findViewById(R.id.notification_text)
        notificationDate = findViewById(R.id.notification_date)
    }

    fun setText(text: String) {
        notificationText.text = text
    }

    fun setDate(date: String) {
        notificationDate.text = date
    }


}