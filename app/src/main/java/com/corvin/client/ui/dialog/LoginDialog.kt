package com.corvin.client.ui.dialog

import android.app.Dialog
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import com.corvin.client.R
import com.dd.processbutton.iml.ActionProcessButton
import com.corvin.client.ui.LoginPhpRunnable
import com.fog.hoh153.database.requestdata.LoginRequestData
import com.fog.hoh153.utils.PreferenceKeys
import com.ms_square.etsyblur.BlurDialogFragment

class LoginDialog : BlurDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window?.run {
            requestFeature(Window.FEATURE_NO_TITLE)
        }
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_login, container, false)
        addOnTouchListener(view)

        val nameField = view.findViewById<EditText>(R.id.login_name_field)
        val passField = view.findViewById<EditText>(R.id.login_password_field)

        val loginButton = view.findViewById<ActionProcessButton>(R.id.login_button)
        loginButton.setMode(ActionProcessButton.Mode.ENDLESS)
        loginButton.setOnClickListener {
            loginButton.progress = 1
            nameField.isEnabled = false
            passField.isEnabled = false
            val loginData = LoginRequestData(nameField.text.toString(), passField.text.toString())
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString(PreferenceKeys.USER_PASSWORD, passField.text.toString()).apply()
            //DatabaseControllerService.sendRequestToDatabase(DatabaseCommand.LOGIN, loginData)
            Thread(LoginPhpRunnable("http://oktat.narasoft.hu/php/DeviceLogin.php", loginData)).start()
        }
        return view
    }

    private fun addOnTouchListener(view: View) {
        view.post {
            view.setOnTouchListener { v, _ ->
                v.performClick()
                false
            }
        }
    }
}
