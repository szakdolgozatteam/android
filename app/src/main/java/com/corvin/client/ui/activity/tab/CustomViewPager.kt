package com.corvin.client.ui.activity.tab

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var enableSwipe: Boolean = false

    init {
        init()
    }

    private fun init() {
        enableSwipe = false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return enableSwipe && super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return enableSwipe && super.onTouchEvent(event)

    }

    fun setEnableSwipe(enableSwipe: Boolean) {
        this.enableSwipe = enableSwipe
    }
}